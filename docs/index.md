## Cloth Face Masks project

***Goal : Making beautiful cloth face masks for the population guided by the latest scientific findings.***

----
- Contributors :
  - Hélène Bardijn, Architect, Fablab ULB, ULB
  - Gwendoline Best, Architect, Fablab ULB, ULB
  - Axel Cornu, Electronician, Frugal LAB - Faculté des Sciences & Fablab ULB, ULB
  - Valentine Fruchart, Engineer, Fabricademy & Fablab ULB
  - Carmen Ionescu, Anestésiste, Hopital d'Ixelles
  - Stéphanie Krins, Physicist, Fablab ULB, ULB
  - Floriane Weyer, Physicist, GRASP, ULiège
- Project Leads :
  - Nicolas De Coster (<ndcoster@meteo.be>), Physicist, IRM & Fablab ULB
  - Victor Levy (<vlevy@ulb.ac.be>), Architect, Faculté d'Architecture & Fablab ULB, ULB
  - Denis Terwagne (<Denis.Terwagne@ulb.ac.be>), Physicist, Frugal LAB - Faculté des Sciences & Fablab ULB, ULB
- International collaborations :
  - [MIT Center for Bits and Atoms (USA)](https://gitlab.cba.mit.edu/pub/coronavirus/tracking/)
  - [Fablab Global Network](https://gitlab.fabcloud.org/pub/project/coronavirus/tracking)
- Documentation editor : Denis Terwagne
- Documentation License: [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)](https://creativecommons.org/licenses/by-nc-sa/4.0/)
- Created : April 20, 2020
- Last updated : May 5, 2020
- Working draft document - Not peer reviewed
---

Numerous medical doctors are encouraging the population to wear face masks to protect themselves and others from the Coronavirus spreading [^1]. In sight of the end of the lockdown in Belgium, wearing a face mask when shopping will most probably become mandatory.

However, masks are sold out !

As a last resort, millions of people in the world are making homemade cloth face masks for their local hospitals, retirement homes, for people on the front line and for themselves.

The different governments have given some guidelines to the population to make them ([Belgium](publications/2020-SPF_masquebuccal_20200318_v2.pdf), [USA](https://www.cdc.gov/coronavirus/2019-ncov/prevent-getting-sick/diy-cloth-face-coverings.html), ...). These masks are not meant to protect people at a 100% but are used as a barrier to slow down spreading of the disease.

Plenty of models exist, beautiful ones, ugly ones, safe ones, unsafe ones, ...
but which one, should you use ?

## Project goals

Here are the goals of this ongoing project :

1. highlighting the best cloth face mask tutorials and designs we have found and tested.
2. highlighting best available filters
3. creating and sharing beautiful new designs that people want to wear.
4. giving design rules based on the latest scientific findings.
5. Building a scientific experimental setup to test the masks efficiency.

## 1. The best tutorials

The [Belgian Federal Public Service for health](https://www.health.belgium.be/en) is recommending this
[this mask](publications/2020-SPF_masquebuccal_20200318_v2.pdf) which has the following particularities :

+ **3 sizes** (man, woman and child)
+ possibility to add a **filter** by below
+ can be made with **elastic bands** or **cotton strings** to attach the mask.
+ need a sewing machine
+ time to make : 15-30 minutes/mask

Here is how to make it :

<iframe width="560" height="315" src="https://www.youtube.com/embed/iSNQTSIQnLo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## 2. What type of filter should we use ?

### sterilisation sheets

the *French Society of Sterilization Sciences* and the *French Society for Hospital Hygiene* have done ***Filtration Efficacy tests*** on [sterilisation sheets SMS (Spunbound-Metblown-Spunbound)](https://www.sf2h.net/wp-content/uploads/2020/02/Avis-SF2S-SF2H-Mate%CC%81riaux-alternatifs-pour-la-confection-de-masques-chirurgicaux.pdf) that are used for packaging and maintaining the sterility of sterilized products.

![](images/SMS.jpg)

The cellulose free SMS sheets are
 - totally hydrophobic (no passage of liquids)
 - allow the passage of gases (air).

<iframe width="560" height="315" src="https://www.youtube.com/embed/QkEWqKntdSc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Barrier Filtration Efficiency [[source](https://www.sf2h.net/wp-content/uploads/2020/02/Avis-SF2S-SF2H-Mate%CC%81riaux-alternatifs-pour-la-confection-de-masques-chirurgicaux.pdf)]

![](images/SMS_FiltrationEfficacy.png)

The [official French database from EuraMaterials](https://euramaterials.eu/masques-anti-projections-les-resultats-des-tests-de-caracterisation-matiere-de-la-dga/) is a guide to help manufacturers identify the right combinations of materials to make cloth face masks. Different materials with different layers have been tested and they have been categorized according to the test results (Nonconforming, Category 1, Category 2).

Here are the results of the tests.

|   | Breathability L /m^2 /s  | Filtration Efficacy % |
|---|---|---|
| Surgical Mask  | 139  | approx 95 %   |
| French limit (Covid-19) for cloth masks  | 96  |   |
| Reliance SMS 400  | 91.8  | x  |
| Reliance SMS 300  | 145  | 97.6 %  |
| Reliance SMS 200  | 182  | 98.6 %  |



## 3. Making beautiful masks ...

We observe, at this point, that lots of people are not wearing masks even if they have some available. It seems that there is a psychological barrier to cross to wear these masks.

A few fashion designers are encouraging the population to wear masks [by making beautiful masks and distributing them freely](https://www.dw.com/fr/le-cr%C3%A9ateur-de-mode-david-ochieng-lance-des-masques-de-protection-contre-le-covid-19/av-53041216).

![](images/FablabULB-BeautifulMask.jpeg)

## 4. ...  guided by the latest scientific findings

But how to make beautiful masks guided by the latest scientific findings ?

We propose here to list a series of recommandations (design constraints) based on the latest scientific literature.

## 5. Building an experimental test bench

We propose here to build 2 experimental bench tests that can be made in a fablab to measure the quality of homemade masks.

 - **Filter material testing** : how well your mask material is filtering nanoparticles.
 - **Fit test**: how well the mask is covering your face and  avoids air leakage.


#### Filter material test

We are building an Henderson apparatus [[Wilkes, 2000](publications/2000-wilkes-filtration.pdf)] to measure the efficiency of the filtering materials. A similar apparatus has already been build at the [MIT CBA](https://gitlab.cba.mit.edu/camblackburn/filter_testing) with whom we are collaborating.

![](./images/2000-Wilkes-Henderson-Apparatus.png)

#### Fit test

We would like to reproduce an experimental bench test similar to the one presented in [this research paper](publications/2010-bowen-facemask.pdf)

![](./images/2010-Bowen-experimental-setup.png)

## Do you want to help ?

To build the 2 above experimental test bench we would need the following items.  

If you can help please send an email at <Denis.Terwagne@ulb.ac.be> and <Axel.Cornu@ulb.ac.be>.

- acrylic tubes
- air flow regulators
- pneumatic fittings and tubes
- a wet-resistant pressure gauge
- a nebulizer
- a particle sampler
- some reference nanoparticles

##### Acrylic tubes specifications

- from 22mm to 150mm
- minimum length 200mm

##### Air flow regulator specifications

- for air, not water or helium
- maximum flow between 10 50 l/min and 50 l/min
- manual (rotameter) or remote controlled

##### Tubing specifications

- 6mm or 1/4" outside diameter
- 4 mm or 1/8" inside diameter
- clear
- food-grade or chemical resistant
- PE (polyethylene), Nylon (polyamide) or Tygon material

##### Pneumatic fittings specifications

- Threaded-to-Tube Adapter
- for 1/4", 1/8" or M5 thread
- for 6mm or 1/4" plug-in tube
- o-rings, from 5mm to 8mm inside diameter

##### Pressure gauge

- differential
- maximum rating up to 1bar or 15psi
- lower pressure is ok
- if possible, temperature-compensated
- wet-resistant

##### Examples of nebulizer

![Collison nebulizer](./images/Collison_nebulizer.jpg)
![Aerosol nebulizer](./images/Aerosol_nebulizer.jpg)

##### Examples of particle sampler

![Cascade impactor particle sampler](./images/Cascade_Impactor.jpg)
![Two-piece impinger](./images/Impinger.png)

##### Nanoparticles specifications

- dispersion in water, or citrate solution
- gold, latex or any solid that can be imaged
- particle size from 20nm to 200nm

## References

##### Fablab ULB Covid-19 projects

- [Fablab ULB Covid-19 Response Projects](https://gitlab.com/fablab-ulb/projects/coronavirus/tracking)

##### Medical information

- [Airflow, Lung Volumes, and Flow-Volume Loop](https://www.msdmanuals.com/professional/pulmonary-disorders/tests-of-pulmonary-function-pft/airflow,-lung-volumes,-and-flow-volume-loop) ([french version](https://www.msdmanuals.com/fr/professional/troubles-pulmonaires/%C3%A9preuves-fonctionnelles-respiratoires-efr/flux-a%C3%A9riens,-volumes-pulmonaires-et-courbe-d%C3%A9bit-volume))

##### Cloth Face Mask tutorials

- [Belgian Federal Public Service for health ](publications/2020-SPF_masquebuccal_20200318_v2.pdf)
- [Center for Disease Control - Covid-19 (USA)](https://www.cdc.gov/coronavirus/2019-ncov/prevent-getting-sick/diy-cloth-face-coverings.html)

##### On-going research and development

- [Prakash Lab COVID-19 Projects](https://docs.google.com/document/d/1EHCbje_mIBrZ9qavTHe8NPvTNKT_CbGv0TXHvzngm2A/edit)
  - [Particle Filtration Efficiency Test Protocol](https://docs.google.com/document/d/1zInS5npxCcTHxyG_4F2spCf274HL31bDbiNRcHQrvNI/)

##### Published papers

- Testing the Efficacy of Homemade Masks: Would They Protect in an Influenza Pandemic? [Original paper](publications/daviesfacemask2013.pdf), [Education version](https://smartairfilters.com/en/blog/diy-homemade-mask-protect-virus-coronavirus/)
- [Does That Face Mask Really Protect You?](publications/bowenfacemask2010.pdf)
- [Simple Respiratory Protection—Evaluation of the
Filtration Performance of Cloth Masks and Common
Fabric Materials Against 20–1000 nm Size Particles](publications/Simple_Respiratory_Protection.pdf)

##### Technical documentation

- (https://smartairfilters.com/en/blog/diy-homemade-mask-protect-virus-coronavirus/)
- https://smartairfilters.com/en/blog/kids-air-pollution-masks-work-effective/
- https://smartairfilters.com/en/blog/pollution-masks-india-air-pollution/

##### Filter materials

- Polyester cloth
- [NaCl activated qualitative filter paper](https://abishekmuthian.com/diy-qualitative-filter-face-mask/)

##### Fit tests

Fit test can be :

- qualitative, see this [3M guide](resources/Qualitative_Fit_Testing_Guide.pdf) [(french version)](resources/Essai_Ajustement_Qualitatif.pdf)
- quantitative (example devices below)

![](./images/Fit_Tester.jpg)

A quantitative fit tester can be made of a single particle counter, according to [this sheet from TSI](resources/Mask_Protection_Assessment_Test_System.pdf).

## Footnotes

[^1]: [Pour le Dr Jean-Luc Gala, chaque citoyen belge doit fabriquer et porter un masque, La Libre Belgique, 30 mars 2020](https://www.lalibre.be/planete/sante/pour-le-dr-jean-luc-gala-chaque-citoyen-belge-doit-fabriquer-et-porter-un-masque-5e821a977b50a6162be3810a)
