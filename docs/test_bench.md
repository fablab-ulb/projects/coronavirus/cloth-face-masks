##  Mask quality test bench

***This is a work in progress, not tested yet***

Low-cost setup, based on the Henderson apparatus [Wilkes, 2000](publications/2000-wilkes-filtration.pdf).

Design inspired by [Camron Blackburn](https://gitlab.cba.mit.edu/camblackburn/filter_testing/-/blob/master/README.md)'s testing instrument.

![Setup diagram](./images/setup.png)

### Aerosol generator

Proto setup :

![Aerosol generator](./images/neb_3.jpg)

To do :

- Cut holes for the air ports
- Airtight case for the aerosol dryer

### Air flow regulators

Specs :

- Device : Brooks Instruments [MR3000](https://www.brooksinstrument.com/en/~/media/brooks/documentation/products/variable%20area%20flow%20meters/plastic/3000/variable-area-flow-meter-data-sheet-3000.pdf)
- Regulator type : rotameter
- Indicator type : variable area
- Dryer air flow range : 0 - 10 l/min
- Nebulizer air flow range : 0 - 7 l/min

#### Air dryer

Crafted with an air filter case and a 3D-printed cap :

![Air dryer, empty](./images/dryer_1.jpg)
![Air dryer, with silica gel](./images/dryer_2.jpg)

#### Nebulizer

![Aerosol nebulizer](./images/neb_1.jpg)
![Aerosol nebulizer, inside view](./images/neb_2.jpg)

Specs :

- Device : Lifecare [L5006](./images/Lifecare.png)
- Max. air flow : 7 l/min
- Max. aerosol rate : 0.34 ml/min
- Mass median aerodynamic diameter : 3.5 µm

#### Aerosol dryer

Inspired from a work on [Aerosolization of Particles from a Bubbling Liquid](publications/1997-Ulevicius-aerosol.pdf).

Temperature and RH will be measured inside the dryer case.

***Under construction***

#### Temperature and RH sensor

Specs :

- Device : Amphenol [T9602-5-D](https://www.amphenol-sensors.com/en/telaire/humidity/527-humidity-sensors/3224-t9602)
- Response time : up to 29 sec

### Spray tube & test rig

Proto setup, shown with particle sampler :

![Test tube, sensors and sampler](./images/tube.jpg)

To do :

- Cut holes for the air probes
- Airtight case for OPC
- Find some pressure reference for calibration

#### Spray tube

Specs :

- Inside diameter : 54 mm
- Wall thickness : 3 mm
- Material : extruded PMMA
- Two ports for differential pressure
- Two probes for OPC

#### Optical particle counter (OPC)

Specs :

- Device : Sensiron [SPS30](https://www.sensirion.com/fileadmin/user_upload/customers/sensirion/Dokumente/9.6_Particulate_Matter/Datasheets/Sensirion_PM_Sensors_SPS30_Datasheet.pdf)
- PM range : 0.3 - 10 μm
- Sample rate : 1 sample/s
- Two isokinetic probes for optical PM count

Probes :

![Isokinetic probes, curing](./images/probe_0.jpg)
![Isokinetic probe](./images/probe_1.jpg)

***Prototype*** of isokinetic probes, 3D printed with Formabs Form2 (SLA).

#### Pressure sensor

Specs :

- Device : Honeywell [26PCAFA6D](https://sensing.honeywell.com/honeywell-sensing-board-mount-pressure-26pc-series-installation-instructions-50047634.pdf)
- Differential pressure range : 0 - 1 psi
- Resistor bridge conditioner : HX711 module
- Sample rate : 10 - 80 sample/s

### Particle sampler

Parts :

- Falcon tube
- 3D-printed cap
- Pasteur pipette
- M5 barbed fitting
- Low-cost diaphragm air pump

![Impinger, top view](./images/imp_1.jpg)
![Impinger, side view](./images/imp_2.jpg)

Specs :

- Vial volume : 15 ml
- Air pump flow : 15 l/min

Setup :

![Impinger and pump](./images/imp_3.jpg)

To do :

- Test air pump flow
- Find an output air filter
