## Cloth Face Masks project

***Goal : Making beautiful cloth face masks for the population that are scientifically proven to be safe.***

----
- Contributors : Fablab ULB members + Frugal LAB
- Project Leads : Denis Terwagne <Denis.Terwagne@ulb.ac.be>, Victor Levy (<vlevy@ulb.ac.be>)  
- Documentation editor : Denis Terwagne
- Documentation License: [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)](https://creativecommons.org/licenses/by-nc-sa/4.0/)
- Created : April 20, 2020
- Last updated : April 22, 2020
- Working draft document - Not peer reviewed
---

Here is a project on Cloth Face Masks that we are developing in Fablab ULB with the help of many collaborators.

---------> See [this website](https://fablab-ulb.gitlab.io/projects/coronavirus/cloth-face-masks/) for more information. <--------
